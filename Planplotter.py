



import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
#



#plotter.close('all')
class animation_visited:
    def __init__(self, pe):
        fig=plt.figure(3)
        fig.set_dpi(100)
        patches=[]
        
        ax = plt.axes(xlim=(0, 10), ylim=(0, 10))
        
        rectangle=plt.Rectangle((1, 1), 5, 5, fc='r')
        obstacle1=plt.Rectangle((1, 0), 1, 1, fc='g')
        circle=plt.Circle((5, 8),1 , fc='y')
        #patches.append(circle)
        #patches.append(rectangle)
        
        #plt.gca().add_patch(circle)
        #plt.gca().add_patch(rectangle)
        circle.center=(0,0)
        rectangle.xy=(2,2)
    def animate_visited(self,visited1):

        
        anim = animation.FuncAnimation(self.fig1, self.animate, 
                                   init_func=self.init, 
                                   frames=50, 
                                   interval=100,
                                   blit=True)
        plotter.show()      
        
        
        
    def init(self):
    #    ax.add_patch(Roomba)
        self.ax.add_patch(self.box)
    #    for i in range(len(poly)):
    #        ax.add_patch(pe.poly[i])
        return self.box
        
    def animate(self,i):
    #    x_r,y_r=Roomba.center
        x_b,y_b=self.box.xy
        x_b=self.visited[i][0]-self.pe.box.w/2
        y_b=self.visited[i][1]-self.pe.box.h/2
    #    circle.center=(x_r,y_r)
                
        self.box.xy=(x_b,y_b)
        return self.box  
    
    
    
    
    
 
    
#def plot_prj(x_min, x_max, y_min, y_max, polygons):
def plot_prj(robot_pos, box_pos1, pe):
    box_pos = np.copy(box_pos1)
    fig2=plt.figure(3)
    plt.axis([pe.x_min, pe.x_max, pe.y_min, pe.y_max]) 
    plt.grid(True)
    box_pos[0]=box_pos[0]-pe.box.w/2
    box_pos[1]=box_pos[1]-pe.box.h/2
    
    for i in range(len(pe.polygons)):
        poly=pe.polygons[i]
        obs=plt.Polygon(poly)
#        Obstacles.append(obs)
        plt.gca().add_patch(obs)    
#    obs1=plotter.Polygon(poly1)
#    plotter.gca().add_patch(obs1)
        
    Roomba=plt.Circle(robot_pos, pe.robot.r , fc='y') 
    box=plt.Rectangle(box_pos, pe.box.w, pe.box.h, fc='r')    
    plt.gca().add_patch(Roomba)
    plt.gca().add_patch(box)
    plt.axis('equal')
    plt.show('all')





