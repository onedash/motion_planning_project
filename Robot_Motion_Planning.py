#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search operations for planning.
'''
import numpy as np
import random
import matplotlib.pyplot as plotter
import math
from collisions_project import PolygonEnvironment
import time

_DEBUG = False

_TRAPPED = 'trapped'
_ADVANCED = 'advanced'
_REACHED = 'reached'

class TreeNode:
    def __init__(self, state, parent=None):
        self.state = state
        self.children = []
        self.parent = parent

    def add_child(self, child):
        self.children.append(child)

class RRTSearchTree:
    def __init__(self, init):
        self.root = TreeNode(init)
        self.nodes = [self.root]
        self.edges = []

    def find_nearest(self, s_query):
        min_d = 1000000 # should be set to None instead
        nn = self.root
        for n_i in self.nodes:
            d = np.linalg.norm(s_query - n_i.state)
            if d < min_d: #set min_d to d if None
                nn = n_i
                min_d = d
        return (nn, min_d)

    def add_node(self, node, parent):
        self.nodes.append(node)
        self.edges.append((parent.state, node.state))
        node.parent = parent
        parent.add_child(node)

    def get_states_and_edges(self):
        states = np.array([n.state for n in self.nodes])
        return (states, self.edges)

    def get_back_path(self, n):
        path = []
        while n.parent is not None:
            path.append(n.state)
            n = n.parent
        path.append(n.state)
        path.reverse()
        return path

class RRT:

    def __init__(self, num_samples, num_dimensions=2, step_length = 1, lims = None,
                 connect_prob = 0.05, collision_func = None):
        '''
        Initialize an RRT planning instance
        '''
        self.K = num_samples
        self.n = num_dimensions
        self.epsilon = step_length
        self.connect_prob = connect_prob

        self.map = collision_func

        # Setup range limits
        self.limits = lims
        if self.limits is None:
            self.limits = []
            for n in xrange(num_dimensions):
                self.limits.append([0,100])
            self.limits = np.array(self.limits)

        self.ranges = self.limits[:,1] - self.limits[:,0]
        self.found_path = False
        
    def checkPushPointReachable(self, init, goal, box):
        '''
        takes in the start and end position of the robot
        returns (plan, cost)
        '''
        #print "init = ", init
        #print "goal = ", goal
        #print "box = ", box
        #if self.map.real_test_collisions(init, box):
            #print 'robot start in collision'
            #return (None, -1)
        if self.map.real_test_collisions(goal, box):
            return(None, -1)
        (possiblePath, pathLength) = self.map.generate_plan_around_box(init, goal, box)
        if possiblePath is not None:
            return (possiblePath, pathLength/self.epsilon)
        path = self.build_rrt_connect(init, goal, box)
        #self.map.draw_plan(path, self)
        if path is None:
            print 'path not found'
            return (path, -1)
        return (path, len(path))

    def build_rrt(self, init, goal, box):
        '''
        Build the rrt from init to goal
        Returns path to goal or None
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        # Build tree and search
        self.T = RRTSearchTree(init)
        self.T2 = RRTSearchTree(goal)
        generatedNodes = 0        
        print "generating nodes"
        while generatedNodes < self.K:
            randomPoint = self.sample()
            currentNode = self.extend(randomPoint, self.T, box)
            if currentNode is not None:
                if (currentNode.state == self.goal).all():
                    return self.T.get_back_path(currentNode)
                elif np.linalg.norm(currentNode.state - self.goal) <= self.epsilon:
                    goalNode = TreeNode(self.goal)
                    self.T.add_node(goalNode, currentNode)
                    return self.T.get_back_path(goalNode)
            generatedNodes += 1
            
        return None
        
    def build_rrt_bidirectional(self, init, goal, box):
        '''
        Build the rrt connect from init to goal
        using bidirectional setup
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        # Build tree and search
        self.T = RRTSearchTree(init)
        self.T2 = RRTSearchTree(goal)
        generatedNodes = 0
        currentTree = self.T
        otherTree = self.T2
        print "generating nodes"
        
        while generatedNodes < self.K:
            randomPoint = self.sample()
            currentNode = self.extend(randomPoint, currentTree, box)
            generatedNodes += 1
            if currentNode is not None:
                previousNode = otherTree.find_nearest(currentNode.state)[0]
                currentTreeNode = currentNode
                while generatedNodes < self.K:
                    currentNode = self.extend(currentTreeNode.state, otherTree, box, closeNode = previousNode)
                    generatedNodes += 1
                    if currentNode is not None:
                        if (currentNode.state == currentTreeNode.state).all():
                            print len(self.T.nodes)
                            print len(self.T2.nodes)
                            result = self.T.get_back_path(self.T.find_nearest(currentTreeNode.state)[0].parent) + list(reversed(self.T2.get_back_path(self.T2.find_nearest(currentTreeNode.state)[0])))
                            self.T.nodes = self.T.nodes + self.T2.nodes
                            self.T.edges = self.T.edges + self.T2.edges
                            return result
                        previousNode = currentNode
                    else:
                        break
            
            if currentTree is self.T:
                currentTree = self.T2
                otherTree = self.T
            else:
                currentTree = self.T
                otherTree = self.T2
            '''
            if len(self.T.nodes) >= len(self.T2.nodes):
                currentTree = self.T
                otherTree = self.T2
            else:
                currentTree = self.T2
                otherTree = self.T
            '''
            
        self.T.nodes = self.T.nodes + self.T2.nodes
        self.T.edges = self.T.edges + self.T2.edges
        return None

    def build_rrt_connect(self, init, goal, box):
        '''
        Build the rrt connect from init to goal
        Returns path to goal or None
        '''
        self.goal = np.array(goal)
        self.init = np.array(init)
        self.found_path = False

        # Build tree and search
        self.T = RRTSearchTree(init)
        generatedNodes = 0
        print "generating nodes"
        
        while generatedNodes < self.K:
            randomPoint = self.sample()
            currentNode = self.extend(randomPoint, self.T, box)
            generatedNodes += 1
            if currentNode is not None:
                if (currentNode.state == self.goal).all():
                    return self.T.get_back_path(currentNode)
                elif np.linalg.norm(currentNode.state - self.goal) <= self.epsilon:
                    goalNode = TreeNode(self.goal)
                    self.T.add_node(goalNode, currentNode)
                    return self.T.get_back_path(goalNode)
                previousNode = currentNode
            else:
                continue
            
            while generatedNodes < self.K:
                currentNode = self.extend(randomPoint, self.T, box, closeNode = previousNode)
                generatedNodes += 1
                if currentNode is not None:
                    if (currentNode.state == self.goal).all():
                        return self.T.get_back_path(currentNode)
                    elif np.linalg.norm(currentNode.state - self.goal) <= self.epsilon:
                        goalNode = TreeNode(self.goal)
                        self.T.add_node(goalNode, currentNode)
                        return self.T.get_back_path(goalNode)
                    elif (currentNode.state == randomPoint).all():
                        break
                    previousNode = currentNode
                else:
                    break
    
        return None

    def sample(self):
        '''
        Sample a new configuration and return
        '''
        # Return goal with connect_prob probability
        
        if random.random() < self.connect_prob:
            return self.goal
        
        result = np.array([random.uniform(x[0], x[1]) for x in self.limits])
        return result

    def extend(self, q, tree, box, closeNode = None):
        '''
        Perform rrt extend operation.
        q - new configuration to extend towards
        '''
        if closeNode is None:
            (closestNode, distance) = tree.find_nearest(q)
        else:
            closestNode = closeNode
        totalDistance = np.linalg.norm(q - closestNode.state)
        
        if totalDistance <= self.epsilon:
            if self.map.real_test_collisions(q, box):
                return None
            node = TreeNode(q)
            tree.add_node(node, closestNode)
            return node
                
        edgeRatio = self.epsilon / totalDistance
        nodePosition = ((closestNode.state) + ((edgeRatio) * (q - closestNode.state)))
        
        if self.map.real_test_collisions(nodePosition, box):
#            print "In Collision"
            return None
                
        node = TreeNode(nodePosition)
        tree.add_node(node, closestNode)
        return node

    def fake_in_collision(self, q):
        '''
        We never collide with this function!
        '''
        return False
