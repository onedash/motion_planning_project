# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 11:53:51 2016

@author: Jacob
"""
import graph_search_solutions as graph
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
#plt.rcParams['animation.ffmpeg_path'] = '/opt/local/bin/ffmpeg'



#g = graph_search.GridMap('./map0.txt',False)
#actions = np.array([0, 1, 2, 3])
#results = graph.a_star_search()



actions = ['0', '1', '2', '3']
filePath = "env_project4.txt"
boxstep = 5
rrtstep = 5
num_RRT_Samples = 4000
RRT_Bias = 0.2
dist2goal = 0.5
[visited, path, pe] = graph.a_star_search(actions, filePath, boxstep, rrtstep, num_RRT_Samples, RRT_Bias, dist2goal)
print "These are the results"
#print results
print path

#Amir=plotter.animation_visited(pe)
#Amir.animate_visited(results)
#plotter.animation_visited.animate_visited(results, pe, plotter.init, plotter.animate)
if path == None:
    print "no path to goal found"
    exit(1)
    
  
    

print 'total visited: ', len(visited)
print 'total unique: ', len(set(visited))
print'unique visited:\n', str(set(visited))
    
################################# figure    1 #######################   
fig=plt.figure(5)
fig.set_dpi(100)
patches=[]
goal_pos=np.copy(pe.goal)
goal_pos[0]=pe.goal[0]-pe.box.w/2
goal_pos[1]=pe.goal[1]-pe.box.h/2
#plotter.axis([pe.x_min, pe.x_max, pe.y_min, pe.y_max]) 
ax = plt.axes(xlim=(pe.x_min,pe.x_max), ylim=(pe.y_min,pe.y_max))

#box=plt.Rectangle((1, 1), 5, 5, fc='r')
box=plt.Rectangle(pe.start, pe.box.w, pe.box.h, fc='r')    
#obstacle1=plt.Rectangle((1, 0), 1, 1, fc='g')
#circle=plt.Circle((5, 8),1 , fc='y')
Roomba=plt.Circle(pe.start_robot, pe.robot.r , fc='y') 
goal=plt.Rectangle(goal_pos, pe.box.w, pe.box.h, fc='g') 
plt.gca().add_patch(goal) 
for i in range(len(pe.polygons)):
        poly=pe.polygons[i]
        obs=plt.Polygon(poly)
#        Obstacles.append(obs)
        plt.gca().add_patch(obs) 




#patches.append(circle)
#patches.append(rectangle)

#plt.gca().add_patch(circle)
#plt.gca().add_patch(rectangle)
#Roomba.center=(0,0)
#box.xy=(2,2)


def init1():
    Roomba.center=pe.start_robot
    box._xy=goal_pos
#    obstacle1.xy=(1,0)
    ax.add_patch(Roomba)
    ax.add_patch(box)
#    ax.add_patch(obstacle1)
    return Roomba,box #,obstacle1

def animate1(i):
    x_r,y_r=Roomba.center
    x_b,y_b=box.xy
    x_b=path[i][1][0]-pe.box.w/2
    y_b=path[i][1][1]-pe.box.h/2
    x_r=path[i][0][0]
    y_r=path[i][0][1]    
    
    box.xy=(x_b,y_b)
    Roomba.center=(x_r,y_r)
    return Roomba,box #,obstacle1    
    
anim = animation.FuncAnimation(fig, animate1, 
                               init_func=init1, 
                               frames=len(path), 
                               interval=100,
                               blit=True)


#anim.save('particle_box.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
#FFwriter = animation.FFMpegWriter()
#anim.save('basic_animation.mp4', writer = FFwriter, fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()


##################################### figure 2 ###################################
#fig2=plt.figure(3)
#fig2.set_dpi(100)
#patches2=[]
##plotter.axis([pe.x_min, pe.x_max, pe.y_min, pe.y_max]) 
#ax2 = plt.axes(xlim=(pe.x_min,pe.x_max), ylim=(pe.y_min,pe.y_max))
#
##box=plt.Rectangle((1, 1), 5, 5, fc='r')
#box2=plt.Rectangle(pe.start, pe.box.w, pe.box.h, fc='r')    
##obstacle1=plt.Rectangle((1, 0), 1, 1, fc='g')
##circle=plt.Circle((5, 8),1 , fc='y')
#Roomba2=plt.Circle(pe.start_robot, pe.robot.r , fc='y') 
#goal2=plt.Rectangle(pe.goal, pe.box.w, pe.box.h, fc='g') 
#plt.gca().add_patch(goal2) 
#for i in range(len(pe.polygons)):
#        poly2=pe.polygons[i]
#        obs2=plt.Polygon(poly2)
##        Obstacles.append(obs)
#        plt.gca().add_patch(obs2) 
#
#
#
#
##patches.append(circle)
##patches.append(rectangle)
#
##plt.gca().add_patch(circle)
##plt.gca().add_patch(rectangle)
#Roomba2.center=(0,0)
#box2.xy=(2,2)
#
#
#def init2():
#    Roomba2.center=(0,0)
#    box2._xy=(2,2)
##    obstacle1.xy=(1,0)
#    ax2.add_patch(Roomba2)
#    ax2.add_patch(box2)
##    ax.add_patch(obstacle1)
#    return Roomba2,box2 #,obstacle1
#
#def animate2(i):
#    x_r2,y_r2=Roomba2.center
#    x_b2,y_b2=box2.xy
#    x_b2=visited[i][0]-pe.box.w/2
#    y_b2=visited[i][1]-pe.box.h/2
#    box2.xy=(x_b2,y_b2)
#    
#    return Roomba2,box2 #,obstacle1    
#    
#anim2 = animation.FuncAnimation(fig2, animate2, 
#                               init_func=init2, 
#                               frames=len(visited), 
#                               interval=100,
#                               blit=True)
#plt.show()

#anim.save('animation.mp4', fps=30, 
#          extra_args=['-vcodec', 'h264', 
#                      '-pix_fmt', 'yuv420p'])
                      
#ffmpeg -i animation.mp4 -r 10 output%05d.png
#
#convert output*.png output.gif
