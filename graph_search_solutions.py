#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search operations for planning.
'''
import numpy as np
import heapq
import matplotlib.pyplot as plotter
from math import hypot, fabs
from Robot_Motion_Planning import RRT
from collisions_project import PolygonEnvironment
from Planplotter import plot_prj as pp

_DEBUG = False
_DEBUG_END = True
_ACTIONS = ['u','d','l','r']
_ACTIONS_2 = ['u','d','l','r','ne','nw','sw','se']
_X = 1
_Y = 0
_GOAL_COLOR = 0.75
_INIT_COLOR = 0.25
_PATH_COLOR_RANGE = _GOAL_COLOR-_INIT_COLOR
_VISITED_COLOR = 0.9
cost_map = {'u': 1,'d': 1,'l': 1,'r': 1,'ne': 1.5,'nw': 1.5,'sw': 1.5,'se': 1.5}

class GridMap:
    '''
    Class to hold a grid map for navigation. Reads in a map.txt file of the format
    0 - free cell, x - occupied cell, g - goal location, i - initial location.
    Additionally provides a simple transition model for grid maps and a convience function
    for displaying maps.
    '''
    def __init__(self, map_path=None, use_cost=False):
        '''
        Constructor. Makes the necessary class variables. Optionally reads in a provided map
        file given by map_path.

        map_path (optional) - a string of the path to the file on disk
        '''
        self.rows = None
        self.cols = None
        self.goals = []
        self.init_pos = None
        self.occupancy_grid = None
        self.use_costs = use_cost
        if map_path is not None:
            self.read_map(map_path)

    def read_map(self, map_path):
        '''
        Read in a specified map file of the format described in the class doc string.

        map_path - a string of the path to the file on disk
        '''
        map_file = file(map_path,'r')
        lines = [l.rstrip().lower() for l in map_file.readlines()]
        map_file.close()
        self.rows = len(lines)
        self.cols = max([len(l) for l in lines])
        if _DEBUG:
            print 'rows', self.rows
            print 'cols', self.cols
            print lines
        self.occupancy_grid = np.zeros((self.rows, self.cols), dtype=np.bool)
        for r in xrange(self.rows):
            for c in xrange(self.cols):
                if lines[r][c] == 'x':
                    self.occupancy_grid[r][c] = True
                if lines[r][c] == 'g':
                    self.goals.append((r,c))
                elif lines[r][c] == 'i':
                    self.init_pos = (r,c)

    def is_goal(self,s):
        '''
        Test if a specifid state is the goal state

        s - tuple describing the state as (row, col) position on the grid.

        Returns - True if s is the goal. False otherwise.
        '''
        for g in self.goals:
            if (s[_X] == g[_X] and
                s[_Y] == g[_Y]):
                return True
        return False



# number of pushpoints_index:
# 0 : up
# 1 : down
# 2 : left
# 3 : right
#box_size = (box_lenth, box_width)
#pushpoint_set=([X,Y],[X,Y],[X,Y],[X,Y])

    



#    def transition(self, s, a):
#        '''
#        Transition function for the current grid map.
#
#        s - tuple describing the state as (row, col) position on the grid.
#        a - the action to be performed from state s
#
#        returns - s_prime, the state transitioned to by taking action a in state s.
#        If the action is not valid (e.g. moves off the grid or into an obstacle)
#        returns the current state.
#        '''
#        new_pos = list(s[:])
#        # Ensure action stays on the board
#        cost = 0.
#        if a == 'u':
#            if s[_Y] > 0:
#                new_pos[_Y] -= 1
#                cost = 1.
#        elif a == 'd':
#            if s[_Y] < self.rows - 1:
#                new_pos[_Y] += 1
#                cost = 1.
#        elif a == 'l':
#            if s[_X] > 0:
#                new_pos[_X] -= 1
#                cost = 1.
#        elif a == 'r':
#            if s[_X] < self.cols - 1:
#                new_pos[_X] += 1
#                cost = 1.
#        elif a == 'ne':
#            if s[_X] < self.cols - 1 and s[_Y] > 0:
#                new_pos[_X] += 1
#                new_pos[_Y] -= 1
#                cost = 1.5
#        elif a == 'nw':
#            if s[_Y] > 0 and s[_X] > 0:
#                new_pos[_Y] -= 1
#                new_pos[_X] -= 1
#                cost = 1.5
#        elif a == 'sw':
#            if s[_Y] < self.rows - 1 and s[_X] > 0:
#                new_pos[_Y] += 1
#                new_pos[_X] -= 1
#                cost = 1.5
#        elif a == 'se':
#            if s[_Y] < self.rows - 1 and s[_X] < self.cols - 1:
#                new_pos[_Y] += 1
#                new_pos[_X] += 1
#                cost = 1.5
#        else:
#            print 'Unknown action:', str(a)
#
#        # Test if new position is clear
#        if self.occupancy_grid[new_pos[0], new_pos[1]]:
#            s_prime = tuple(s)
#            cost = 0.
#        else:
#            s_prime = tuple(new_pos)
#        if self.use_costs:
#            return s_prime, cost
#        else:
#            return s_prime

    def display_map(self, path=[], visited={}):
        '''
        Visualize the map read in. Optionally display the resulting plan and visisted nodes

        path - a list of tuples describing the path take from init to goal
        visited - a set of tuples describing the states visited during a search
        '''
        display_grid = np.array(self.occupancy_grid, dtype=np.float32)

        # Color all visited nodes if requested
        for v in visited:
            display_grid[v] = _VISITED_COLOR
        # Color path in increasing color from init to goal
        for i, p in enumerate(path):
            disp_col = _INIT_COLOR + _PATH_COLOR_RANGE*(i+1)/len(path)
            display_grid[p] = disp_col

        display_grid[self.init_pos] = _INIT_COLOR
        for goal in self.goals:
            display_grid[goal] = _GOAL_COLOR

        # Plot display grid for visualization
        imgplot = plotter.imshow(display_grid)
        # Set interpolation to nearest to create sharp boundaries
        imgplot.set_interpolation('nearest')
        # Set color map to diverging style for contrast
        imgplot.set_cmap('spectral')
        plotter.show()

    def euclidean_heuristic(self, s):
        '''
        Euclidean heuristic function

        s - tuple describing the state as (row, col) position on the grid.

        returns - floating point estimate of the cost to the goal from state s
        '''
        h = []
        for goal in self.goals:
            h.append(hypot(s[0]-goal[0],s[1]-goal[1]))
        return min(h)

    def manhattan_heuristic(self, s):
        '''
        Euclidean heuristic function

        s - tuple describing the state as (row, col) position on the grid.

        returns - floating point estimate of the cost to the goal from state s
        '''
        h = []
        for goal in self.goals:
            h.append(fabs(s[0]-goal[0]) + fabs(s[1]-goal[1]))
        return min(h)

    def uninformed_heuristic(self, s):
        '''
        Example of how a heuristic may be provided. This one is admissable, but dumb.

        s - tuple describing the state as (row, col) position on the grid.

        returns - floating point estimate of the cost to the goal from state s
        '''
        return 0.0


class SearchNode:
    def __init__(self, s, A, parent=None, parent_action=None, cost=0, depth=0, startRobot = None, path = None):
        '''
        s - the state defining the search node
        A - list of actions
        parent - the parent search node
        parent_action - the action taken from parent to get to s
        '''
        self.parent = parent
        self.cost = cost
        self.parent_action = parent_action
        self.state = s[:]
        self.actions = A
        self.depth = depth
        self.robotStart = startRobot[:]
        self.path = path

    def __str_full__(self):
        return str(self.state) + ' ' + str(self.actions)+' '+str(self.parent)+' '+str(self.parent_action)
    def __str__(self):
        return str(self.state) + ' ' + str(self.cost)

class PriorityQ:
    '''
    Priority queue implementation with quick access for membership testing
    Setup currently to only with the SearchNode class
    '''
    def __init__(self):
        '''
        Initialize an empty priority queue
        '''
        self.l = [] # list storing the priority q
        self.s = set() # set for fast membership testing

    def __contains__(self, x):
        '''
        Test if x is in the queue
        '''
        tupleState = (x.state[0], x.state[1])
        return tupleState in self.s

    def push(self, x, cost):
        '''
        Adds an element to the priority queue.
        If the state already exists, we update the cost
        '''
        tupleState = (x.state[0], x.state[1])
        if tupleState in self.s:
            return self.replace(x, cost)
        heapq.heappush(self.l, (cost, x))
        self.s.add(tupleState)

    def pop(self):
        '''
        Get the value and remove the lowest cost element from the queue
        '''
        x = heapq.heappop(self.l)
        tupleState = (x[1].state[0], x[1].state[1])
        self.s.remove(tupleState)
        return x[1]

    def peak(self):
        '''
        Get the value of the lowest cost element in the priority queue
        '''
        x = self.l[0]
        return x[1]

    def __len__(self):
        '''
        Return the number of elements in the queue
        '''
        return len(self.l)

    def replace(self, x, new_cost):
        '''
        Removes element x from the q and replaces it with x with the new_cost
        '''
        xTupleState = (x.state[0], x.state[1])
        for y in self.l:
            yTupleState = (y[1].state[0], y[1].state[1])
            if xTupleState == yTupleState:
                self.l.remove(y)
                self.s.remove(yTupleState)
                break
        heapq.heapify(self.l)
        self.push(x, new_cost)

    def get_cost(self, x):
        xTupleState = (x.state[0], x.state[1])
        for y in self.l:
            yTupleState = (y[1].state[0], y[1].state[1])
            if xTupleState == yTupleState:
                return y[0]

    def __str__(self):
        s = ''
        for n in self.l:
            s += '('+str(n[0])+', '+str(n[1])+')' + ', '
        s = '['+s[:-2]+']'
        return s

def backpath(node):
    '''
    Function to determine the path that lead to the specified search node

    node - the SearchNode that is the end of the path

    returns - a tuple containing (path, action_path) which are lists respectively of the states
    visited from init to goal (inclusive) and the actions taken to make those transitions.
    '''
    path = []
    action_path = []
    while node.parent is not None:
        path.append(node.state)
        action_path.append(node.parent_action)
        node = node.parent
    path.reverse()
    action_path.reverse()
    return (path, action_path)

def bfs_search_map(init_state, f, is_goal, actions):
    '''
    Perform breadth first search on a grid map.

    init_state - the intial state on the map
    f - transition function of the form s_prime = f(s,a)
    is_goal - function taking as input a state s and returning True if its a goal state
    actions - set of actions which can be taken by the agent

    returns - ((path, action_path), visited) of None if no path can be found
    path - a list of tuples. The first element is the initial state followed by all states 
    traversed until the final goal state
    action_path - the actions taken to transition from the initial state to goal state
    '''
    n0 = SearchNode(init_state, actions)
    Q = [n0]  # Search queue
    visited = set()
    visited.add(init_state)
    while len(Q) > 0:
        n_i = Q.pop(0)
        for a in n_i.actions:
            s_prime = f(n_i.state, a)
            n_prime = SearchNode(s_prime, actions, n_i, a)
            if is_goal(s_prime):
                if _DEBUG_END:
                    print 'goal found at', s_prime
                return (backpath(n_prime), visited)
            elif s_prime not in visited:
                Q.append(n_prime)
                visited.add(s_prime)
    if _DEBUG_END:
        print 'No goal found'
    return None

def uniform_cost_search(init_state, f, is_goal, actions):
    cost = 0
    n0 = SearchNode(init_state, actions, cost=cost)
    frontier = PriorityQ()
    frontier.push(n0, cost)
    visited = set()
    while len(frontier) > 0:
        n_i = frontier.pop()
        if is_goal(n_i.state):
            if _DEBUG_END:
                print 'goal found at', n_i.state
                print 'cost to goal', n_i.cost
            return backpath(n_i), visited
        visited.add(n_i.state)
        for a in n_i.actions:
            s_prime, action_cost = f(n_i.state, a)
            new_cost = n_i.cost + action_cost
            n_prime = SearchNode(s_prime, actions, n_i, a, cost = new_cost)
            if s_prime not in visited and s_prime not in frontier:
                frontier.push(n_prime, new_cost)
            elif s_prime in frontier and new_cost < frontier.get_cost(n_prime):
                frontier.push(n_prime, new_cost)
                # frontier.replace(n_prime, new_cost)
    if _DEBUG_END:
        print 'No goal found'
    return None, visited

def a_star_search(actions, filePath, boxstep, rrtstep, num_RRT_samples, RRTBias, dist2goal):
    pe = PolygonEnvironment()
    pe.read_env(filePath)
#    pp(pe.start_robot, pe.start, pe)
    dims = len(pe.start)
    rrt = RRT(num_RRT_samples,
                  dims,
                  rrtstep,
                  lims = pe.lims,
                  connect_prob = RRTBias,
                  collision_func=pe)
    
    queue = PriorityQ()
    #Start = (pe.start[0], pe.start[1])
    
    n0 = SearchNode(pe.start, None, parent=None, parent_action=None, cost=0, depth=0, startRobot = pe.start_robot, path = None)
    queue.push(n0,0)
    visited = []
    path=[]
    nodes_visited = 0
    while len(queue) > 0:
        print "Length of Queue = ", len(queue)
        n_i = queue.peak()
        node_cost = queue.get_cost(n_i)
        n_i = queue.pop()
        nodes_visited = nodes_visited + 1
        visited.append((n_i.state[0], n_i.state[1]))
        if np.linalg.norm([pe.goal[0]-n_i.state[0], pe.goal[1]-n_i.state[1]]) < dist2goal:
            #while 1:
            print "WE FOUND GOAL"
            #return [visited, None, pe]
        #if is_goal(n_i.state):
            #path.append(n_i.state)
            PathList = []
            loopCount = 0
            while n_i.parent != None:
                print "loopCount=", loopCount
                loopCount = loopCount+1
                print "parentPath", n_i.path
                for i in range(len(n_i.path)-1, -1, -1):
                    pathValue = n_i.path[i] 
                    boxValue = n_i.parent.state
                    PathList.append([pathValue, boxValue])
                n_i = n_i.parent
            PathList = list(reversed(PathList))
            return [visited, PathList, pe]
                    
#                path.append(n_i.parent.state)
#                n_i = n_i.parent
#            path = list(reversed(path))
#            return (path, visited)
        
        if nodes_visited >= 5000:
            return [visited, None, pe]
        
        for a in actions:
            [NewBoxLoc, RobotStateInit, RobotStateFinal] = transition_funcion(a, n_i.state, boxstep, [pe.box.w, pe.box.h], pe.robot.r)
#            pp(RobotStateFinal, NewBoxLoc, pe)
#            pp(RobotStateInit, NewBoxLoc, pe)
            if pe.test_collisions_box(NewBoxLoc) ==  False:
                [plan, Cost2push] = rrt.checkPushPointReachable(n_i.robotStart, RobotStateInit, n_i.state)
#                print plan
               
                if plan is not None:
                    Cost2Go = Cost2push+n_i.cost
                    heuristic = hypot(NewBoxLoc[0]-pe.goal[0],NewBoxLoc[1]-pe.goal[1])
                    #print "action = ", a, "NewBoxLoc = ", NewBoxLoc, "heuristic = ", heuristic                    
                    TotalCost = heuristic + Cost2Go
                    
                    n_prime = SearchNode(NewBoxLoc, a, parent=n_i, parent_action=None, cost=Cost2Go, depth=0, startRobot = RobotStateFinal, path = plan)
                    tstate = (n_prime.state[0], n_prime.state[1])                    
                    if tstate in visited:
                        continue
                    if n_prime not in queue:
                        queue.push(n_prime,TotalCost)
                    elif queue.get_cost(n_prime)>TotalCost:
                        queue.replace(n_prime,TotalCost)
    return [None,None,None]
                    #n_prime = SearchNode(s_prime, actions, n_i, a)
#
#    
#'''
#    
#Frontiere(BoxState, Costs to goal (x,y), cost to get there(path Length Robot), Cost to goal (theta))
#	Add initial state to Frontiere
#	While( Frontiere not empty)
#		Current State = Pop lowest total cost node
#            if(is_goal) (within some epsilon)
#                Path = findPath(Current State)
#                Return path
#            For possible actions
#<<<<<<< HEAD
#                [NewBoxLocation, New RobotLocation] = TransisitionFunction(pushpoint, box location, stepsize)
#=======
#                [NewBoxLocation, New RobotLocation] = transisition_function(pushpoint_index, box_state, stepsize, , box_size, robot_radius)
#>>>>>>> 07ddd95d9557ce05c805bdeaad1602319813b338
#                if(checkCollision(NewBoxLocation, New RobotLocation) ==  true)
#                    [plan, Cost2push] = checkPushpointReachable(robot location, box location, pushpoint)) {You figure out the goal}
#                    if(plan not none)
#                        Calculate TotalCost = (Cost2push + currentCost) + heuristic(whatever it is)
#                        Add New Node(plan, state, parent)
#                        Add to Frontiere[newBoxLocation, NewRobotLocation, TotalCost, CurrentCost]	
#	Return None
#'''
def transition_funcion(pushpoint_index, box_state1, stepsize, box_size, robot_radius):
    robot_state_initial = []
    robot_state_final = []
    box_state = np.copy(box_state1)
    
    if pushpoint_index is '0':
        robot_state_initial.append(box_state[0])
        robot_state_initial.append(box_state[1]+box_size[1]/2+robot_radius)
        
        box_state[0]=box_state[0]
        box_state[1]=box_state[1]-stepsize
                
        robot_state_final.append(box_state[0])     
        robot_state_final.append(box_state[1]+box_size[1]/2+robot_radius)
    
    elif pushpoint_index is '1':
        robot_state_initial.append(box_state[0])
        robot_state_initial.append(box_state[1]-box_size[1]/2-robot_radius)
        
        box_state[0]=box_state[0]
        box_state[1]=box_state[1]+stepsize        
        
        robot_state_final.append(box_state[0])
        robot_state_final.append(box_state[1]-box_size[1]/2-robot_radius)
    
    elif pushpoint_index is '2':
        robot_state_initial.append(box_state[0]-box_size[0]/2-robot_radius)   
        robot_state_initial.append(box_state[1])   
        
        box_state[0]=box_state[0]+stepsize
        box_state[1]=box_state[1]
        
        robot_state_final.append(box_state[0]-box_size[0]/2-robot_radius)     
        robot_state_final.append(box_state[1])
    
    elif pushpoint_index is '3':
        robot_state_initial.append(box_state[0]+box_size[0]/2+robot_radius)  
        robot_state_initial.append(box_state[1])        
        
        box_state[0]=box_state[0]-stepsize
        box_state[1]=box_state[1]        
        
        robot_state_final.append(box_state[0]+box_size[0]/2+robot_radius)      
        robot_state_final.append(box_state[1])
    return (box_state, robot_state_initial, robot_state_final)  

